import sys
from PyQt5.QtWidgets import *
# from PyQt5.QtGui import *

class Window(QMainWindow):
	def __init__(self):
		super().__init__()
		self.setGeometry(50,50,500,300)
		self.setWindowTitle("Menue bar exer")
		self.setMenu()

	def setMenu(self):
		# 建立menu
		self.menu = self.menuBar()
		# 建立一個選項叫做File
		fileMenu = self.menu.addMenu('&File')

		# 建立動作名稱Exit
		quitAction = QAction('&Exit', self)
		# 快捷鍵
		quitAction.setShortcut("Ctrl+Q")
		# 當這個被點到的時候 連接到close()這個方法,,, 注意 不能寫成close()
		quitAction.triggered.connect(self.close)
		# 把exit加到file裡面
		fileMenu.addAction(quitAction)

		# 建立動作名稱NewFile
		newFileAction = QAction('&New File', self)
		# 快捷鍵
		newFileAction.setShortcut("Ctrl+N")
		# 當這個被點到的時候 連接到__init__()這個方法,,, 注意 不能寫成__init__()
		newFileAction.triggered.connect(self.__init__)
		# 把exit加到file裡面
		fileMenu.addAction(newFileAction)		

		# create action named save
		saveAction = QAction('&Save', self)
		# shortcut
		saveAction.setShortcut("Ctrl+S")
		# 亂打一個函數也可以哦...
		saveAction.triggered.connect(lambda: print("Save"))
		# add action to file
		fileMenu.addAction(saveAction)

		self.show()

	def close(self):
		sys.exit()
		

if __name__ == '__main__':
	app = QApplication(sys.argv)
	GUI = Window()
	sys.exit(app.exec_())