import sys
from PyQt5.QtWidgets import *

class Demo(QMainWindow):

	def __init__(self):
		super().__init__()
		self.title = '0401_Exercise'
		self.left = 10
		self.top = 10
		self.width = 640
		self.height = 480
		self.initUI()


	def initUI(self):
		self.setWindowTitle(self.title)
		self.setGeometry(self.left, self.top, self.width, self.height)
		self.initButton()
		self.initMenu()
		self.show()

	def initButton(self):
		quitBtn = QPushButton('Quit', self)
		# quitBtn.clicked.connect(QApplication.instance().quit)
		quitBtn.clicked.connect(self.close)
		# quitBtn.resize(quitBtn.sizeHint())
		quitBtn.resize(50, 50)
		quitBtn.move(100,125)


		self.numBtn = {}
		for i in range(1,9):
			self.numBtn[i] = QPushButton('{}'.format(i), self)
			self.numBtn[i].resize(50, 50)
			self.numBtn[i].move(((i-1)%3)*50,((i-1)//3)*50+25)


		leftBtn = QPushButton('Left', self)
		leftBtn.clicked.connect(lambda: QMessageBox.about(self, 'Left', 'This is left'))
		leftBtn.resize(leftBtn.sizeHint())
		leftBtn.move(150,150)

		rightBtn = QPushButton('Right', self)
		rightBtn.clicked.connect(lambda: QMessageBox.about(self, 'Right', 'This is right'))
		rightBtn.resize(rightBtn.sizeHint())
		rightBtn.move(225,150)



	def initMenu(self):
		menu = self.menuBar()

		fileMenu = menu.addMenu("&File")
		quitAction = QAction("&Exit", self)
		quitAction.setShortcut("Ctrl+Q")
		# quitAction.triggered.connect(qApp.quit)
		quitAction.triggered.connect(self.close)
		fileMenu.addAction(quitAction)

		editMenu = menu.addMenu("&Edit")
		copyAction = QAction("Copy", self)
		copyAction.setShortcut("Ctrl+C")
		copyAction.triggered.connect(lambda: print("You've copied the text!"))
		editMenu.addAction(copyAction)

		viewMenu = menu.addMenu("&View")
		enlargeAction = QAction("Enlarge", self)
		enlargeAction.triggered.connect(lambda: self.setGeometry(2*self.left, 2*self.top, 2*self.width, 2*self.height))
		viewMenu.addAction(enlargeAction)

		preferencesMenu = menu.addMenu("&Preferences")
		settingAction = QAction("Setting", self)
		preferencesMenu.addAction(settingAction)

		helpMenu = menu.addMenu("&Help")
		sendAction = QAction("Send Report", self)
		helpMenu.addAction(sendAction)

	def close(self, event):

		reply = QMessageBox.warning(self, 'Message', "quit or not ?", QMessageBox.Ok | QMessageBox.No, QMessageBox.No)

		if reply == QMessageBox.Ok:
			sys.exit()
		else:
			None

if __name__ == '__main__':
	app = QApplication(sys.argv)
	GUI = Demo()
	sys.exit(app.exec_())